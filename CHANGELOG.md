## 0.19.0 (2023-05-18)

### added (2 changes)

- [something technical](dat.pham2/changelogdemo@818c57aa175454c86d96c41d12d4d7cb53ba5b9e)
- [Added new file](dat.pham2/changelogdemo@fe7b729f68bc1419faa149f16f403f6bdf2d66dd)

## 0.18.0 (2023-05-18)

### added (1 change)

- [Added hi y'all](dat.pham2/changelogdemo@00285456671385a4fc94a840b34fc5453904997f)

### changed (1 change)

- [Separate release notes and changelog entries](dat.pham2/changelogdemo@369f32f334a69b75e61d67c08a081399d784a541)

## 0.17.0 (2023-05-17)

### fix (1 change)

- [Fix release notes formatting using echo -e](dat.pham2/changelogdemo@521b335bec239ef0753b825c0239121d1b6a180b)

## 0.16.0 (2023-05-17)

### fix (1 change)

- [Fix release notes formatting using print](dat.pham2/changelogdemo@7fff1d190b259fcf28f94bfddd39f5f98618bd0d)

## 0.15.0 (2023-05-17)

### fix (1 change)

- [Fix release notes formatting once again, now using printf](dat.pham2/changelogdemo@37dd77b19a228963a232a9f6ca3fa0762bb9b661)

## 0.14.0 (2023-05-17)

### fix (1 change)

- [Fix release notes formatting again](dat.pham2/changelogdemo@964a30abe36588cbe92f85a5597019d5d929bf6c)

## 0.13.0 (2023-05-16)

### fix (1 change)

- [Fix release notes formatting](dat.pham2/changelogdemo@df1f25f70a943933ddf8e9f27fe2a269bd618cb9)

## 0.12.0 (2023-05-16)

### fix (1 change)

- [Fix tag rule](dat.pham2/changelogdemo@7f854f051fcdfd072813b6b54c03fd78f505a2b0)

### added (1 change)

- [Added good morning](dat.pham2/changelogdemo@be83f386e6311307ed09edd380e1c32079bf4eeb)

## 0.11.0 (2023-05-16)

### added (2 changes)

- [Automate release notes](dat.pham2/changelogdemo@a7efd0ae8add7823f70071004e397909eb001899)
- [Add goodbye me](dat.pham2/changelogdemo@e8b0d445e8ada64e18166a81a52cf222c14b40be)

## 0.10.0 (2023-05-04)

### added (2 changes)

- [Add hi dat](dat.pham2/changelogdemo@ce778b47a201a286905f0570b9e19be389bb57bf)
- [Add hello world](dat.pham2/changelogdemo@ccbeae2a9518db121662861063148d36875465f2)

## 0.9.0 (2023-05-04)

### fixed (1 change)

- [Fix changelog not written to main and dev](dat.pham2/changelogdemo@ba432355891da043036f14c0c5d64e6c3a3ce773)

## 0.3.0 (2023-03-09)

### added (1 change)

- [Added ci file](dat.pham2/changelogdemo@279c04cf0fd9721e0fedefab9a6e3b8c1576fe4c)

## 0.1.0 (2023-03-08)

### added (1 change)

- [Last update: 8.3.2023, 11:03](dat.pham2/changelogdemo@c20cffd62194432e7e016ea1f092a33384d8284c)
